import React, { Component } from "react";
import Posts from "./components/Posts";

import { fetchTopStories } from "./utils/api";

class TopStories extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        loading: true
      },
      isActive: false,
      selected: null
    };
  }

  componentDidMount() {
    fetchTopStories().then(posts =>
      this.setState({
        data: {
          loading: false,
          posts
        }
      })
    );
  }

  _handleChange = (e, selectedValue) => {
    e.preventDefault();

    const { isActive } = this.state;
    this.setState({
      isActive: !isActive,
      selected: selectedValue
    });
  };

  render() {
    const { loading, posts } = this.state.data;
    const { isActive, selected } = this.state;

    if (loading) return <p className="loading"> Loading... </p>;
    return (
      <article>
        {posts && (
          <>
            <h1 className="page__title"> Top Stories </h1>
            <Posts
              data={posts}
              handleChange={this._handleChange}
              selected={selected}
              active={isActive}
            />
          </>
        )}
      </article>
    );
  }
}

export default TopStories;
