import React from "react";
import TopStories from "./TopStories";
import Header from "./components/Header";
import Footer from "./components/Footer";

import "./index.scss";

const App = () => {
  return (
    <>
      <Header />
      <main className="container" role="main">
        <TopStories />
      </main>
      <Footer />
    </>
  );
};

export default App;
