import React from "react";
import PropTypes from "prop-types";

const Posts = ({ data, handleChange, selected, active }) => (
  <ul className="l-grid">
    {data &&
      data.map(story => (
        <li
          key={story.id}
          className={`l-grid__item c-card ${story.id == selected &&
            active &&
            "is-active"}`}
          onClick={e => handleChange(e, story.id)}
        >
          <div className="c-card__wrapper">
            <a className="c-card__title" href={story.url}>
              {story.title}
            </a>

            {selected == story.id && active && (
              <>
                <span>
                  by <b>{story.by} </b>
                </span>
                <span>
                  joined <b>{formatDate(story.time)} </b>
                </span>
                <span>
                  Score <b>{story.score} </b>
                </span>
              </>
            )}
          </div>
        </li>
      ))}
  </ul>
);
export default Posts;

Posts.propTypes = {
  data: PropTypes.array.isRequired,
  handleChange: PropTypes.func,
  selected: PropTypes.number,
  active: PropTypes.bool
};

function formatDate(timestamp) {
  return new Date(timestamp * 1000).toLocaleDateString("en-GB", {
    hour: "numeric",
    minute: "numeric"
  });
}
