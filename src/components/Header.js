import React from "react";

const Header = () => (
  <header className="header">
    <div className="nav__left">
      <a className="header__logo" href="">
        <img src="#" alt="" />
      </a>
      <nav className="nav">
        <ul>
          <li className="nav__item">
            <a className="nav__link" href="#">
              link
            </a>
          </li>
          <li className="nav__item">
            <a className="nav__link" href="#">
              link
            </a>
          </li>
          <li className="nav__item">
            <a className="nav__link" href="#">
              link
            </a>
          </li>
        </ul>
      </nav>
    </div>

    <a className="header__logo header__logo--small " href="">
      <img src="#" alt="" />
    </a>
  </header>
);
export default Header;
