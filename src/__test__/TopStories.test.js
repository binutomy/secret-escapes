/* eslint-disable */
import React from "react";
import Adapter from "enzyme-adapter-react-16";
import {
    shallow,
    configure
} from "enzyme";
import TopStories from "../TopStories";

configure({
    adapter: new Adapter()
});

beforeAll(() => {
    global.fetch = jest.fn();
});
let wrapper;
beforeEach(() => {
    wrapper = shallow( < TopStories /> , {
        disableLifecycleMethods: true
    });
});
afterEach(() => {
    wrapper.unmount();
});
it("must render a loading before api call success", () => {
    expect(wrapper.find(".loading").exists()).toBeTruthy();
});
it("must show the user and hide the loading span after api call success",
    (done) => {
        // here we are spying on componentDidMount to know that it has been called
        const spyDidMount = jest.spyOn(TopStories.prototype, "componentDidMount");
        fetch.mockImplementation(() => {
            return Promise.resolve({
                status: 200,
                json: () => {
                    return Promise.resolve({
                        userName: "binu",
                        userId: 1
                    });
                }
            });
        });
        const didMount = wrapper.instance().componentDidMount();
        // expecting componentDidMount have been called
        expect(spyDidMount).toHaveBeenCalled();
        return didMount.then(() => {
            // updating the wrapper
            wrapper.update();
            expect(wrapper.find(".page__title").text()).toContain("Top Stories");
            expect(wrapper.find(".loading").length).toBe(0);
            spyDidMount.mockRestore();
            fetch.mockClear();
            done();
        });
    });
