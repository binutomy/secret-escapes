/* eslint-disable */
import React from "react";
import ReactDOM from "react-dom";
import Adapter from "enzyme-adapter-react-16";
import { shallow, configure } from "enzyme";
import Header from "../components/Header";

configure({ adapter : new Adapter() });

describe("Header component", () => {
  test("renders without crashing", () => {
    const wrapper = shallow(<Header />);
    expect(wrapper.find(".nav__left .nav")).toHaveLength(1);
    expect(wrapper.find(".nav__item")).toHaveLength(3);
  });
  
});
