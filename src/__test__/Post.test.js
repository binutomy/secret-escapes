/* eslint-disable */
import React from "react";
import ReactDOM from "react-dom";
import Adapter from "enzyme-adapter-react-16";
import {
    shallow,
    configure
} from "enzyme";
import Posts from "../components/Posts";

configure({
    adapter: new Adapter()
});

const sampleData = [{
    by: "skovorodkin",
    descendants: 25,
    id: 21631116,
    score: 236,
    time: 1574706187,
    title: "Computer Architecture – ETH Zürich – Fall 2019",
    type: "story",
    url: "https://safari.ethz.ch/architecture/fall2019/doku.php?id=schedule"
}];

const wrapper = shallow(<Posts data={sampleData} />)

describe("Posts component", () => {
    afterAll(() => wrapper.unmount());

  it("should render a card if props present", () => {
    expect(wrapper.find("ul.l-grid")).toHaveLength(1);
    expect(wrapper.find(".c-card__title").text()).toContain(sampleData[0].title);
    expect(wrapper.find(".c-card__title").prop("href")).toBe(sampleData[0].url);
  });

});
