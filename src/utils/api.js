const api = "https://hacker-news.firebaseio.com/v0";

export function fetchItem(id) {
  return fetch(`${api}/item/${id}.json`).then(res => res.json());
}

export function fetchTopStories() {
  return fetch(`${api}/topstories.json`)
    .then(res => res.json())
    .then(ids => Promise.all(ids.map(fetchItem)))
    .then(posts => posts);
}
